package domain;

import java.util.Scanner;

public class TrocoCliente {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Digite o valor do produto: ");
		Double valorProduto = scanner.nextDouble();
		
		System.out.print("Digite o valor do dinheiro do cliente: ");
		Double valorCliente = scanner.nextDouble();
		
		Boolean trocoCliente = valorCliente >= valorProduto;
		
		if(trocoCliente) {
			valorCliente -= valorProduto;
			System.out.println("Troco: " + valorCliente);
		} else {
			valorCliente -= valorProduto;
			System.out.println("Falta: " + valorCliente);
		}
		
		scanner.close();
	}

}
